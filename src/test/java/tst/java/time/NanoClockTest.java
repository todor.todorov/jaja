/*
 * The MIT License
 *
 * Copyright 2020 Todor Todorov (todorst@yahoo.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package tst.java.time;

import static java.time.ZoneOffset.UTC;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Todor Todorov (todor.todorov@atos.net)
 */
class NanoClockTest {
	private static final ZoneId UTC_2 = ZoneId.of("UTC+2");

	@BeforeAll static void setUpClass() {
	}

	@AfterAll static void tearDownClass() {
	}

	@BeforeEach
	void setUp() {
	}

	@AfterEach
	void tearDown() {
	}

	@Test
	void testGetZone() {
		System.out.println("getZone");
		Clock instance = NanoClock.nanoUTC();
		ZoneId expResult = UTC;
		ZoneId result = instance.getZone();
		assertEquals(expResult, result);
	}

	@Test
	void testWithZone() {
		System.out.println("withZone");
		ZoneId zone = UTC_2;
		Clock instance = NanoClock.nanoUTC();
		Clock expResult = NanoClock.nano(zone);
		Clock result = instance.withZone(zone);
		assertEquals(expResult, result);
		assertNotSame(expResult, result);

		result = expResult.withZone(zone);
		assertSame(expResult, result);

		assertThrows(NullPointerException.class, () -> instance.withZone(null));
	}

	@Test
	void testInstant() {
		System.out.println("instant");
		Clock instance = NanoClock.nanoUTC();
		Instant before = Instant.EPOCH.plusNanos(System.nanoTime());
		final Instant result = instance.instant();
		Instant after = Instant.EPOCH.plusNanos(System.nanoTime());
		assertTrue(result.isAfter(before));
		assertTrue(result.isBefore(after));

		final int dev = result.compareTo(before);
		assertTrue(0 < dev && dev < 50_000, () -> "deviation between 0 and 50 000 ns, but was " + dev);
		final int dev2 = result.compareTo(after);
		assertTrue(-50_000 < dev2 && dev2 < 0, () -> "deviation between -50 000 and 0 ns, but was " + dev2);
	}

	@Test
	void testHashCode() {
		System.out.println("hashCode");
		Clock instance = NanoClock.nanoUTC();
		int expResult = 259;
		int result = instance.hashCode();
		assertEquals(expResult, result);
	}

	@Test
	void testEquals() {
		System.out.println("equals");
		Clock instance = NanoClock.nano(UTC);
		assertFalse(instance.equals(Clock.systemUTC()));
		assertFalse(instance.equals(Clock.systemDefaultZone()));
		assertFalse(instance.equals(Clock.system(UTC)));
		assertFalse(instance.equals(NanoClock.nano(UTC_2)));
		assertTrue(instance.equals(NanoClock.nano(UTC)));
		final Clock utc = NanoClock.nanoUTC();
		assertTrue(instance.equals(utc));
		assertTrue(utc.equals(instance));
		final Clock utc2 = NanoClock.nano(UTC);
		assertTrue(instance.equals(utc2));
		assertTrue(utc2.equals(instance));
		assertTrue(utc.equals(utc2));
		assertTrue(utc2.equals(utc));
	}

	@Test
	void testToString() {
		System.out.println("toString");
		String result = NanoClock.nanoUTC().toString();
		assertEquals("NanoClock{zone=Z}", result);

		result = NanoClock.nano(UTC_2).toString();
		assertEquals("NanoClock{zone=UTC+02:00}", result);
	}

	@Test
	void testNanoUTC() {
		System.out.println("nanoUTC");
		Clock result = NanoClock.nanoUTC();
		assertNotNull(result);
		assertEquals(UTC, result.getZone());
	}

	@Test
	void testNanoDefaultZone() {
		System.out.println("nanoDefaultZone");
		Clock result = NanoClock.nanoDefaultZone();
		assertNotNull(result);
		assertEquals(ZoneId.systemDefault(), result.getZone());
	}

	@Test
	void testNano() {
		System.out.println("nano");
		ZoneId zone = UTC_2;
		Clock result = NanoClock.nano(zone);
		assertNotNull(result);
		assertEquals(zone, result.getZone());

		assertThrows(NullPointerException.class, () -> NanoClock.nano(null));
	}
}
