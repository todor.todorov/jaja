/*
 * The MIT License
 *
 * Copyright 2020 Todor Todorov (todorst@yahoo.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package tst.java.time;

import java.io.Serializable;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Objects;

/**
 *
 * @author Todor Todorov, created on 4.04.2020
 */
public final class NanoClock extends Clock implements Serializable {
	private static final long serialVersionUID = 7714925474990310706L;
	private final ZoneId zone;

	private NanoClock(ZoneId zone) {
		this.zone = Objects.requireNonNull(zone, "zone must not be null");
	}

	@Override
	public ZoneId getZone() {
		return zone;
	}

	@Override
	public Clock withZone(ZoneId zone) {
		Objects.requireNonNull(zone, "zone must not be null");
		if (Objects.equals(zone, this.zone)) {
			return this;
		}
		return new NanoClock(zone);
	}

	@Override
	public Instant instant() {
		return Instant.EPOCH.plusNanos(System.nanoTime());
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 37 * hash + Objects.hashCode(this.zone);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final NanoClock other = (NanoClock) obj;
		return Objects.equals(this.zone, other.zone);
	}

	@Override
	public String toString() {
		return "NanoClock{" + "zone=" + zone + '}';
	}

	public static Clock nanoUTC() {
		return new NanoClock(ZoneOffset.UTC);
	}

	public static Clock nanoDefaultZone() {
		return new NanoClock(ZoneId.systemDefault());
	}

	public static Clock nano(ZoneId zone) {
		return new NanoClock(zone);
	}
}
